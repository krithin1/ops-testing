echo "Killing previous fio"

for ctrl in node{1..20}; do echo $ctrl; ssh $ctrl "pkill fio"; done

ssh node1 "nohup /pdtarget/fio --direct=1 --rw=randread --iodepth=32 --numjobs=4 --bs=12k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/rnvmed1n1:/dev/rnvmed2n1 &" 2>&1 &

ssh node2 "nohup /pdtarget/fio --direct=1 --rw=randread --iodepth=32 --numjobs=4 --bs=12k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/rnvmed3n1:/dev/rnvmed4n1 &" 2>&1 &

ssh node3 "nohup /pdtarget/fio --direct=1 --rw=randread --iodepth=32 --numjobs=4 --bs=12k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/rnvmed5n1:/dev/rnvmed6n1 &" 2>&1 &

ssh node4 "nohup /pdtarget/fio --direct=1 --rw=randread --iodepth=32 --numjobs=4 --bs=12k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/rnvmed7n1:/dev/rnvmed8n1 &" 2>&1 &

ssh node5 "nohup /pdtarget/fio --direct=1 --rw=randread --iodepth=32 --numjobs=4 --bs=12k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/rnvmed9n1:/dev/rnvmed10n1 &" 2>&1 &

ssh node6 "nohup /pdtarget/fio --direct=1 --rw=randread --iodepth=32 --numjobs=4 --bs=12k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/rnvmed37n1:/dev/rnvmed38n1 &" 2>&1 &

ssh node7 "nohup /pdtarget/fio --direct=1 --rw=randread --iodepth=32 --numjobs=4 --bs=12k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/rnvmed39n1:/dev/rnvmed40n1 &" 2>&1 &

ssh node8 "nohup /pdtarget/fio --direct=1 --rw=randread --iodepth=32 --numjobs=4 --bs=12k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/rnvmed41n1:/dev/rnvmed42n1 &" 2>&1 &

ssh node9 "nohup /pdtarget/fio --direct=1 --rw=randread --iodepth=32 --numjobs=4 --bs=12k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/rnvmed43n1:/dev/rnvmed44n1 &" 2>&1 &

ssh node10 "nohup /pdtarget/fio --direct=1 --rw=randread --iodepth=32 --numjobs=4 --bs=12k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/rnvmed45n1:/dev/rnvmed46n1 &" 2>&1 &

echo "Started all IO"


for ctrl in node{1..20}; do echo $ctrl; ssh $ctrl "ps -ef | grep -c fio"; done
