echo "Killing previous fio"

for ctrl in node{1..4}; do echo $ctrl; ssh $ctrl "pkill fio"; done

for ctrl in node{11..14}; do echo $ctrl; ssh $ctrl "pkill fio"; done

ssh node1 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms1 &" 2>&1 &

ssh node2 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms2 &" 2>&1 &

ssh node3 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms3 &" 2>&1 &

ssh node4 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms4 &" 2>&1 &

#sh node5 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms5 &" 2>&1 &

ssh node11 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms6 &" 2>&1 &

ssh node12 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms7 &" 2>&1 &

ssh node13 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms8 &" 2>&1 &

ssh node14 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms9 &" 2>&1 &

#sh node15 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms10 &" 2>&1 &

echo "Started all IO"


for ctrl in node{1..20}; do echo $ctrl; ssh $ctrl "ps -ef | grep -c fio"; done
