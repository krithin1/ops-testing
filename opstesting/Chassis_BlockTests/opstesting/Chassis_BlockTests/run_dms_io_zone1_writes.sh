echo "Killing previous fio"

for ctrl in node{1..20}; do echo $ctrl; ssh $ctrl "pkill fio"; done

ssh node1 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms1 &" 2>&1 &

ssh node2 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms2 &" 2>&1 &

ssh node3 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms3 &" 2>&1 &

ssh node4 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms4 &" 2>&1 &

ssh node5 "nohup /pdtarget/fio --direct=1 --rw=write --iodepth=128 --numjobs=1 --bs=1024k --ioengine=libaio --group_reporting=1 --runtime=6000 --time_based=1 --name=izod --filename=/dev/dms5 &" 2>&1 &

echo "Started all IO"


for ctrl in node{1..20}; do echo $ctrl; ssh $ctrl "ps -ef | grep -c fio"; done
