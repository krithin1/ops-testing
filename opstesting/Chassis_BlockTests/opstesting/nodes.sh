if [ -z "$1" ]
then
      echo "Please provide WACA range"
      echo "Command Syntax: $0 <start waca number> <end waca number> <cmd>"
      exit
fi
if [ -z "$3" ]
then
      echo "Please provide script for ssh"
      echo "Command Syntax: $0 <start waca number> <end waca number> <cmd>"
      exit
fi


for i in $( seq $1 $2) 
do
echo node$i:
ssh node$i /bin/bash < $3
echo node$(($i +10)):
ssh node$(($i +10)) /bin/bash < $3
done
