NOTE: pvl_sda_utility.py v0.4

Usage Instructions:
1. The tar ball containing this package needs to be placed, and
   extracted on the Active Mgmt card.
2. Following are the additional command line options supported:

   Arguments:
   -h, --help        show this help message and exit
   --cardid          card id which needs to be upgraded [1-20 incase of Controllers, 1-2 incase of Management]
   --mgmtcheck       check size and version of SDA component on mgmt cards
   --ctlrcheck       check size and version of SDA component on controller cards
   --mgmtupgrade     upgrade the FW of the SDA component on mgmt cards. User needs to provide the cardid parameter.
   --ctlrupgrade     upgrade the FW of the SDA component on ctrl cards. User needs to provide the cardid parameter.
   --chassisupgrade  all controllers and management card(s) shall be upgraded. The chassis shall be reloaded post that.
   --h               Utility usage instructions
   --user            username for accessing the chassis [OPTIONAL]
   --password        password for accessing the chassis [OPTIONAL]

ALERT:
In the case of "mgmtupgrade", the required "cardid" param needs to be provided
In the case of "ctlrupgrade", the required "cardid" param needs to be provided
In the case of "mgmtupgrade", and 'ctlrupgrade', following the upgrade, the said card shall undergo a Reboot.
In the case of "chassisupgrade", all the components shall be upgraded, post which the chassis shall be reloaed.


