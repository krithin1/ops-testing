for i in $( seq $1 $2 ) 
do
echo drive${i}:
/usr/local/acadia/adm/nvme fw-download /dev/nvmed${i}n1 --fw=$3
/usr/local/acadia/adm/nvme fw-activate /dev/nvmed${i}n1 --action=1
done
