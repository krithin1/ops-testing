if [ -z "$1" or -z "$2" or -z "$3"]
then
echo "Please enter all parameters"

echo "command format $0 <waca start> <waca end> <cmd>"
fi

echo mgmt1: 
ssh mgmt1 $3
echo mgmt2: 
ssh mgmt2 $3
for i in $(seq $1 $2)
do 
echo node$i:
ssh node$i $3
echo node$(($i+10)):
ssh node$(($i+10)) $3
done
