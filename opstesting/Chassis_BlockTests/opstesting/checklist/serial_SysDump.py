import sys
import subprocess


def dump(dr, c):
    try:
        q = subprocess.check_output(c+" %d " % dr, shell=True)
        r = q.decode("utf-8")
        return r
    except:
        return 0


def wacadump():
    print("----WACA----")
    cmd = "/usr/local/acadia/bin/atmelutil 27"
    for waca in range(1, 21):
        ct = dump(waca, cmd)
        if ct == 0:
            print("Waca number : " + str(waca) + " is not set with a valid serial number")
        else:
            print("Waca number " + str(waca) + " : " + ct)


def drivedump():
    print("----Drive----")
    cmd = "/usr/local/acadia/bin/atmelutil 1 drive"
    for drive in range(72):
        ct = dump(drive, cmd)
        if ct == 0:
            print("Drive number : " + str(drive) + " is not set with a valid serial number")
        else:
            print("Drive number " + str(drive) + " : " + ct)


def mgmtdump():
    print("----Management----")
    cmd = "/usr/local/acadia/bin/atmelutil 1 misc"
    for mgmt in range(2):
        ct = dump(mgmt, cmd)
        if ct == 0:
            print("Management number : " + str(mgmt) + " is not set with a valid serial number")
        else:
            print("Management number " + str(mgmt) + " : " + ct)


def fabricdump():
    print("----Fabric----")
    cmd = "/usr/local/acadia/bin/atmelutil 1 misc"
    for fab in range(2,4):
        ct = dump(fab, cmd)
        if ct == 0:
            print("Fabric number : " + str(fab) + " is not set with a valid serial number")
        else:
            print("Fabric number " + str(fab) + " : " + ct)


mgmtdump()
fabricdump()
gabba = dump(14, "/usr/local/acadia/bin/atmelutil 1 misc")
print("-----Gabba-----")
if gabba == 0:
    print("Back plane is not set with a valid serial number")
else:
    print("Back plane : " + gabba)
drivedump()

"""
    if sys.argv[1] == "ch":
        cmd = "/usr/local/acadia/bin/atmelutil 1 drive"
        if 0 < int(sys.argv[2]) <= 72:
            dri = int(sys.argv[2])
            s = 0
        else:
            er = 1
            print("Drives should be in range 1 to 72 \n\n")
    elif sys.argv[1] == "wc":

        if 0 < int(sys.argv[2]) <= 20:
            s = 1
            dri = int(sys.argv[2]) + 1
        else:
            er = 1
            print("Argument expected to be in range 1 to 20 \n\n")
    else:
        er = 1
        print(" Arguments should be either wc or ch ")
if er == 1:
    print("Format is : serial_dump <ch|wc|> <number of drives > <File>")
    sys.exit()


for x in range(s, dri):
    ct = dump(x, cmd)
    if ct == 0:
        print("Drive number : "+str(x)+" is not set with a valid serial number")
    else:
        print("Drive number "+str(x)+" : "+ct)
        if len(sys.argv) >= 3:
            out = open(sys.argv[3], 'a')
            out.write(ct)
            out.close()"""