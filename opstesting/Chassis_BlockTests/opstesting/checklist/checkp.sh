echo " This script should be run only on mgmt1 sda1 "
mkdir /mnt
mount /dev/sda2 /mnt
ssh mgmt2 " mkdir /mnt; mount /dev/sda2 /mnt"
echo ****************************************************checking import os****************************************************:
echo mgmt1:sda1:
cat /usr/local/acadia/python/operations/restore_factory/disable_auto_failover.py | grep "import os"
echo mgmt1:sda2:
cat /mnt/usr/local/acadia/python/operations/restore_factory/disable_auto_failover.py | grep "import os"
echo mgmt2:sda1:
ssh mgmt2 "cat /usr/local/acadia/python/operations/restore_factory/disable_auto_failover.py | grep 'import os'"
echo mgmt1:sda1:
ssh mgmt2 "cat /mnt/usr/local/acadia/python/operations/restore_factory/disable_auto_failover.py | grep 'import os'"
echo ****************************************************checking dateutil****************************************************:
echo mgmt1:sda1:
ls /usr/lib/python3.5/site-packages/ | grep date 
echo mgmt1:sda2:
ls /mnt/usr/lib/python3.5/site-packages/ | grep date
echo mgmt2:sda1:
ssh mgmt2 "ls /usr/lib/python3.5/site-packages/ | grep date"
echo mgmt1:sda1:
ssh mgmt2 "ls /mnt/usr/lib/python3.5/site-packages/ | grep date"

