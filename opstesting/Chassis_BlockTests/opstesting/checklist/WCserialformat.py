import sys
import subprocess


def cmd(c):
    try:
        q = subprocess.check_output(c, shell=True)
        r = q.decode("utf-8")
        return r
    except:
        return 0


def waca_serial_format(s):
    f2 = s.split()
    f2.insert(1, '	')
    if f2[2][0:2] == "WC":
        temp = f2[2]
        f2[2] = f2[0]
        f2[0] = temp
    return " ".join(f2)


# a.append(" ".join(f2))

def waca_serial(s, e):
    cmd("rm -rf test.txt")
    for i in range(s, e + 1):
        cmd("/usr/local/acadia/bin/atmelutil 27 %d | grep WC >> test.txt" % i)
        cmd("/usr/local/acadia/bin/atmelutil 27 %d | grep WC >> test.txt" % (i + 10))
    f = open("test.txt", 'r')
    f1 = f.readlines()
    for x in range(len(f1)):
        wtxt = waca_serial_format(f1[x])
        print(wtxt)

if __name__ == "__main__":

    waca_serial(int(sys.argv[1]), int(sys.argv[2]))

    du = "df -h | grep sda1"
    sdd = "lsblk | grep sda | grep disk"
    mem = "cat /proc/meminfo | grep -i memtot"
