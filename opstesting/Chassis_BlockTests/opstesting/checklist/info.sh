
echo "Mellanox:"
./do.sh $1 $2 "ethtool -i eth3 | grep -i firmware" | grep  PVL | cut -c 18-48
echo "Disk usage :"
./do.sh $1 $2 "df -h | grep sda1 " | grep '%' |cut -c 33-38
echo "SDA size :"
./do.sh $1 $2 "lsblk  | grep -i sda | grep disk " | grep G | cut -c 23-28
echo "BIOS version:"
./do.sh $1 $2 "dmidecode  | grep -i version" | grep '5.11' | cut -c 11-16
echo "Memory info:"
./do.sh $1 $2 "cat /proc/meminfo | grep -i memtot " | grep Mem | cut -c 17-28
echo "FPGA:"
./do.sh $1 $2 "/usr/local/acadia/bin/pd_lattice" | grep -i vers | cut -c 22-25
echo "CPU info :"
./do.sh $1 $2 "cat /proc/cpuinfo | grep -i mhz  | grep -c 229"
echo "NTP:"
./do.sh $1 $2 "pacman  -Q | grep -i ntp " | grep -i ntp
