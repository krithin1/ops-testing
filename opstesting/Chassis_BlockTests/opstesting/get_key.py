#!/usr/bin/env python
import argparse
from kmip import enums
from kmip.core.factories import attributes
from kmip.pie import client
from kmip.core import config_helper

def get_secret_key(HOST=None, PORT=None, USERNAME=None, PASSWORD=None, VOLUMENAME=None, CERTFILE=None, CERTKEY=None, CA_CERT=None, CLIENT_CONFIG_FILE_PATH=None):
    c = None
    try:
        if CLIENT_CONFIG_FILE_PATH:
            c = client.ProxyKmipClient(
                config_file = CLIENT_CONFIG_FILE_PATH, config='client')
        else:
            c = client.ProxyKmipClient(
                hostname = HOST,
                port = PORT,
                cert = CERTFILE,
                key = CERTKEY,
                ca = CA_CERT,
                config_file = CLIENT_CONFIG_FILE_PATH,
                ssl_version = "PROTOCOL_SSLv23",
                config = 'client',
                username = USERNAME,
                password = PASSWORD)
    except Exception as e:
        print("Error getting KMIP client due to exception {}", str(e))
    if c is None:
        print("Error getting KMIP client")
        return
    try:
        with c:
            af = attributes.AttributeFactory()
            name_attr = af.create_attribute(
                enums.AttributeType.NAME, VOLUMENAME)
            uids = c.locate(attributes=[name_attr])
            uid = uids[0]
            hostnqn = c.get(uid)
            return hostnqn
    except Exception as e:
        print("Error getting hostnqn due to exception {}", str(e))


def main():
    parser = argparse.ArgumentParser(
        description='Find hostnqn based on configuration file or given configuration parameters ')
    subparsers = parser.add_subparsers(dest='action')
    confile = subparsers.add_parser('config_file')
    confile.add_argument('-client_conf_file', type=str, required=True,
                         help='Path to client configuration file (optional)', default=None)
    confile.add_argument('-volname', type=str, required=True,
                         help='Volume name for which hostnqn needs to be found out')

    manually = subparsers.add_parser('config')
    manually.add_argument('-username', type=str, required=True,
                          help='Username for KMIP server account')
    manually.add_argument('-password', type=str, required=True,
                          help='Password for KMIP server account')
    manually.add_argument(
        '-host', type=str, required=True, help='KMIP server ip')
    manually.add_argument('-port', type=str, required=True,
                          help='KMIP server port')
    manually.add_argument('-certfile', type=str, required=True,
                          help='Path to the  server certificate file', default=None)
    manually.add_argument('-keyfile', type=str, required=True,
                          help='Path to the  server corresponding private key file', default=None)
    manually.add_argument('-ca_cert', type=str, required=True,
                          help='Path to the CA certificate', default=None)
    manually.add_argument('-volname', type=str, required=True,
                          help='Volume name for which hostnqn needs to be found out')

    args = parser.parse_args()
    if args.action == 'config_file':
        sec_key = get_secret_key(
            CLIENT_CONFIG_FILE_PATH=args.client_conf_file, VOLUMENAME=args.volname)
    if args.action == 'config':
        sec_key = get_secret_key(args.host, args.port, args.username, args.password,
                                 args.volname, args.certfile, args.keyfile, args.ca_cert)
    if sec_key:
        print("KMS generated hostnqn for volume {} is: {}".format(
            args.volname, sec_key))
    else:
        print("Couldnt fetch hostnqn from KMS")


if __name__ == "__main__":
    main()
