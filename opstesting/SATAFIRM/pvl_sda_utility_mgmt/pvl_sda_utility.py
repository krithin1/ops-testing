# v0.4: Wrapper script around the SDA Upgrade utility
# Utility Wrappper script to allow for:
# -- retrieval of the FW version of the SDA component on both Ctrls and Mgmts
# -- upgrade of the FW version on both the Ctrls and Mgmts

import argparse
import json
import os
import subprocess
import sys
import tarfile
import pvlclient
import logging
import requests
import datetime
import time
from datetime import datetime

time_zone = None
pvlLibHandle = None
EXT_PKGS='/data/external/ext_pkgs.json'
ERROR=[]
latestsdaver="SSA16E"

def print_error(msg):
    global ERROR
    ERROR.append(msg)
    #print("\033[31mERROR: %s \033[m"%msg)

def print_error_report():
    global ERROR
    if len(ERROR) > 0:
        print ("\033[31m\n***** ERROR REPORT ***** \033[m")
        for i in ERROR:
                print("\033[31mERROR: %s \033[m"%i)
    else:
        print ("\n***** No ERROR reported *****")

def setup_pvlclient(username, passord):
    global pvlLibHandle
    logger = logging.getLogger('test')
    hdlr = logging.FileHandler('/tmp/pvlclient.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    pvlLibHandle = pvlclient.New(u'2.0', u'127.0.0.1', 443, logger=logger, keep_alive=True)
    result = pvlLibHandle.auth.login(user=username, password=passord)
    #print (dir(result))

def execute_cmd(cmd , controller=None):

    stdout = None

    retcode = None
    try:
        def execute(cmd_str):
            #print ('Executing COMMAND %s'%cmd_str)
            result = subprocess.Popen(cmd_str ,shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            stdout = result.communicate()[0]
            retcode = result.returncode
            return retcode, (stdout.decode("utf-8")).strip().split('\n')

        if controller:
            cmd_str= 'ssh %s %s'%(controller,cmd)
            return execute(cmd_str)
        else:
            return execute(cmd)

    except  Exception as E:
        print (E)
        return retcode, stdout

def get_passive_mgmt():
    cmd = '/usr/local/acadia/bin/atmelutil  3'
    retcode, stdout = execute_cmd(cmd)
    if retcode==0 and  'Slot:0' in stdout :
        return ('mgmt2')
    elif retcode==0 and  'Slot:1' in stdout :
        return ('mgmt1')
    else:
        return ('error')

def reload_chassis():

    payload={"chassis_reboot_time": 32, "force_full": 'No'}

    # By default, the call is synchronous
    pvlLibHandle.system.generic_api('POST', '/chassis/poweroff', payload)


def power_cycle_mgmt(mgmtnode=None):

    if mgmtnode:
        print("Standby Management card is being power cycled.")
        standbymgmt=int(mgmtnode[4])

        payload={"chassis_reboot_time": 32, "power": 'off', 'mgmt_num': int(mgmtnode[4])}

        # By default, the call is synchronous
        pvlLibHandle.system.generic_api('POST', '/chassis/poweroff', payload)
    else:
        print("Active Management card is being power cycled.")
        cmd = 'echo b > /proc/sysrq-trigger'
        retcode, stdout = execute_cmd(cmd)


def power_off_controllers(ctlr_list):
    ctlrs_to_power_off_list=[]

    for ctlr in ctlr_list:
        ctlrs_to_power_off_list.append(ctlr['slot'])

    payload={"device_list":ctlrs_to_power_off_list}

    # By default, the call is synchronous
    pvlLibHandle.system.generic_api('POST', '/chassis/controllers/poweroff', payload)


def power_on_controllers(ctlr_list):
    ctlrs_to_power_on_list=[]

    for ctlr in ctlr_list:
        ctlrs_to_power_on_list.append(ctlr['slot'])

    payload={"device_list":ctlrs_to_power_on_list}

    # By default, the call is synchronous
    pvlLibHandle.system.generic_api('POST', '/chassis/controllers/poweron', payload)


def get_all_controllers(print_sum=True):
    result = pvlLibHandle.system.generic_api('GET', '/chassis/controllers?type=1')
    if result.error_code  != 0 :
        print_error("Failed to get controller Info")
        assert(result.error_code  == 0)

    ctlr_list=[]
    if print_sum:
        print ('\nController Summary\n')
    for ctlr in result.payload:
        node= "Node%s"%ctlr['id']
        print ("Node%s    : %s"%(ctlr['id'],ctlr['displayState']))
        if ctlr['displayState'] == 'Active' :
            ctlr_list.append(ctlr)
    return ctlr_list

def get_sda_size_on_mgmt(mgmt=None):

    #if mgmt :
    #    print("Checking SDA Size on Standby Management : %s"%mgmt)
    #else:
    #    print("Checking SDA Size on Active Management ")
    cmd="/tmp/sda_utility/usr/bin/hdparm -I /dev/sda | grep GB | awk '{print $9}' | tr -d '('"

    retcode, stdout = execute_cmd(cmd, mgmt)
    if retcode!=0 :
        str1= "Failed to get version "
        print_error(str1)
        assert(retcode==0)

    print("Size    of SDA partition : %s GB"%stdout[0])

def get_sda_version_on_mgmt(mgmt=None):
    print("----------------------------------------------")

    if mgmt :
        print("Checking SDA properties on Standby Management : %s"%mgmt)
    else:
        print("Checking SDA properties on Active Management")

    cmd="/tmp/sda_utility/usr/bin/hdparm -I /dev/sda | grep Firmware | awk '{print $3}'"

    retcode, stdout = execute_cmd(cmd, mgmt)
    if retcode!=0 :
        str1= "Failed to get version "
        print_error(str1)
        assert(retcode==0)

    currentsdaver=stdout[0]

    print("Version of SDA partition : %s"%stdout[0])

    # Raise an Alert Message incase version does not match desired version 
    if(currentsdaver != latestsdaver):
        if mgmt :
            print_error("\nStandby Management card %s at older SDA FW Version"%mgmt) 
        else:
            print_error("\nActive Management card at Older SDA FW version") 

def install_sda_util_on_mgmt(mgmt=None):

    if mgmt :
        #print("\nInstalling SDA utils on Standby Mgmt : %s"%mgmt)
        cmd='scp sda_utility.tar {}:/tmp'.format(mgmt)
    else:
        #print("\nInstalling SDA utils on Active Mgmt")
        cmd='cp sda_utility.tar /tmp'

    retcode, stdout = execute_cmd(cmd)
    if retcode!=0 :
        str1= "Failed to copy."
        print_error(str1)
        assert(retcode==0)
    #print("SDA Util transferred successfully")

    # Now, untar the package
    cmd='tar -xf /tmp/sda_utility.tar -C /tmp'

    retcode, stdout = execute_cmd(cmd,mgmt)
    if retcode!=0 :
        str1= "Failed to untar "
        print_error(str1)
        assert(retcode==0)

    # Now, install the hdparm package on the Mgmt card
    cmd='sh /tmp/sda_utility/sda_firmware_util.sh --install '

    retcode, stdout = execute_cmd(cmd,mgmt)
    if retcode!=0 :
        str1= "Failed to install hdparm "
        print_error(str1)
        assert(retcode==0)

def transfer_sda_fw_image_on_mgmt(mgmt=None):

    if mgmt :
        print("\nTransferring SDA image on Standby Mgmt : %s"%mgmt)
        cmd='sh /tmp/sda_utility/sda_firmware_util.sh --transfer'

        execute_cmd(cmd,mgmt)
        print("SDA FW Image transferred on mgmt %s."%mgmt)
    else:
        print("\nTransferring SDA image on Active Mgmt.")
        cmd='sh /tmp/sda_utility/sda_firmware_util.sh --transfer'

        execute_cmd(cmd)

def upgrade_sda_fw_image_on_mgmt(mgmt=None):

    if mgmt :
        print("\nUpgrading SDA image on Standby Mgmt : %s"%mgmt)
        cmd='sh /tmp/sda_utility/sda_firmware_util.sh --transfer'

        execute_cmd(cmd,mgmt)
        print("SDA FW Image upgraded on mgmt %s . The node will undergo a reboot now. Please wait..."%mgmt)
    else:
        print("\nUpgrading SDA image on Active Mgmt not permitted using this utility.")
        print("Issue below command manually on Active Mgmt to perform the upgrade. This will lead to failovers.")
        print("sh /tmp/sda_utility/sda_firmware_util.sh --upgrade")
        print("ALERT : In the case Active Mgmt card reboot is stuck, recover using Atmel commands.")

def get_sda_size_on_controller(node, donotprint=None):

    cmd="/tmp/sda_utility/usr/bin/hdparm -I /dev/sda | grep GB | awk '{print $9}' | tr -d '('"

    retcode, stdout = execute_cmd(cmd, node)
    if retcode!=0 :
        str1= "Failed to get version "
        print_error(str1)
        assert(retcode==0)

    if donotprint is None:
        print("Size    of SDA partition on %s : %s GB"%(node, stdout[0]))

def get_sda_version_on_controller(node,donotprint=None):

    cmd="/tmp/sda_utility/usr/bin/hdparm -I /dev/sda | grep Firmware | awk '{print $3}'"

    retcode, stdout = execute_cmd(cmd, node)
    if retcode!=0 :
        str1= "Failed to get version "
        print_error(str1)
        assert(retcode==0)

    currentsdaver=stdout[0]

    if donotprint is None:
        print("----------------------------------------------")
        print("Version of SDA partition on %s : %s"%(node, stdout[0]))

    # Raise an Alert Message incase version does not match desired version
    if(currentsdaver != latestsdaver):
        print_error("\nController %s at older SDA FW version"%node)

def install_sda_util_on_controller(node, donotprint=None):
    #print("\n\nInstalling sda utils on controller %s"%node)

    cmd='scp sda_utility.tar {}:/tmp'.format(node)

    retcode, stdout = execute_cmd(cmd)
    if retcode!=0 :
        str1= "Failed to scp on node"
        print_error(str1)
        assert(retcode==0)

    #if donotprint is None:
    #    print("SDA Util transferred for controller %s"%node)

    # Now, untar the package
    cmd='tar -xf /tmp/sda_utility.tar -C /tmp'

    retcode, stdout = execute_cmd(cmd,node)
    if retcode!=0 :
        str1= "Failed to untar on node"
        print_error(str1)
        assert(retcode==0)

    # Now, install the hdparm package on the Ctrl card
    cmd='sh /tmp/sda_utility/sda_firmware_util.sh --install '

    retcode, stdout = execute_cmd(cmd,node)
    if retcode!=0 :
        str1= "Failed to install hdparm on node"
        print_error(str1)
        assert(retcode==0)

def upgrade_sda_fw_image_on_controller(node):
    print("----------------------------------------------")
    print("Transferring SDA FW to controller %s"%node)

    cmd='sh /tmp/sda_utility/sda_firmware_util.sh --transfer '.format(node)

    retcode, stdout = execute_cmd(cmd, node)
    if retcode!=0 :
        str1= "Failed to upgrade SDA FW Image"
        print_error(str1)
        assert(retcode==0)
    print("SDA FW Image transferred successfully to %s."%node)

def controller_upgrade(ctlr_list):
    for ctlr in ctlr_list:
        node='node'+ctlr['id']
        upgrade_sda_fw_image_on_controller(node)

    print("----------------------------------------------")


def controller_check(ctlr_list, donotprint=None):
    for ctlr in ctlr_list:
        node='node'+ctlr['id']
        install_sda_util_on_controller(node, donotprint)
        get_sda_version_on_controller(node, donotprint)
        get_sda_size_on_controller(node, donotprint)
    print("----------------------------------------------")
    #print("\n\nDone with postcheck on controllers")

def mgmt_check():
        # First, perform actions on Active Mgmt card, followed by Standby Mgmt
        install_sda_util_on_mgmt()
        get_sda_version_on_mgmt()
        get_sda_size_on_mgmt()
        standby=get_passive_mgmt()
        cmd="ping -c 1 {} > /dev/null".format(standby)
        retcode, stdout = execute_cmd(cmd,standby)
        if retcode!=0 :
            print("----------------------------------------------")
            str1= "There is no Standby Mgmt card  %s ."%(standby)
            print_error(str1)
        else:
            install_sda_util_on_mgmt(standby)
            get_sda_version_on_mgmt(standby)
            get_sda_size_on_mgmt(standby)
            print("----------------------------------------------")


def help_summary():
    print("pvl_sda_utility.py v0.4")
    print("--h              : Utility usage instructions")
    print("--cardid         : ID of card to be upgraded. [1-20 incase of Controllers, 1-2 incase of Management]")
    print("--mgmtcheck      : Check Size, Version of SDA component on Management cards.")
    print("--ctlrcheck      : Check Size, Version of SDA component on Controller cards")
    print("--mgmtupgrade    : Upgrade FW of SDA component on Standby Management card. cardid needs to be provided.")
    print("--ctlrupgrade    : Upgrade FW of SDA component on Controller cards. cardid needs to be provided.")
    print("--chassisupgrade : Upgrade FW of SDA component on Management and active Controller cards. Chassis shall be reloaded post upgrade.")



def main():
    parser = argparse.ArgumentParser(description='pvl_sda_utility v0.4')
    parser.add_argument("--cardid",type=int,help="ID of the card to be upgraded. [1-20 incase of Controllers, 1-2 incase of Mgmt Card")
    parser.add_argument("--mgmtcheck",action = 'store_true',help="Check Size, Version of SDA component on Management cards.")
    parser.add_argument("--ctlrcheck",action = 'store_true',help="Check Size, Version of SDA component on Controller cards.")
    parser.add_argument("--mgmtupgrade",action = 'store_true',help="Upgrade FW of SDA component on Standby Management card.")
    parser.add_argument("--ctlrupgrade",action = 'store_true',help="Upgrade FW of SDA component on Controller cards.")
    parser.add_argument("--chassisupgrade",action = 'store_true',help="Upgrade FW of SDA component on Management and Controller cards. Chassis shall be reloaded post upgrade.")
    parser.add_argument("--h",action = 'store_true',help="SDA Utility Usage Instructions")
    parser.add_argument("--user",action = 'store_true', default='admin', help="[OPTIONAL] chassis user name")
    parser.add_argument("--password",action = 'store_true', default='admin', help="[OPTIONAL] chassis password")
    args = parser.parse_args()
    setup_pvlclient(args.user,args.password)

    if args.ctlrcheck:
        # Check the SDA Size and Version on SDA Component
        ctlr_list = get_all_controllers()
        controller_check(ctlr_list)

        print_error_report()

    elif args.mgmtcheck:
        # We need to install the hdparm binary even for reading the version!
        mgmt_check()

        print_error_report()

    elif args.ctlrupgrade:
        # Determine the controller which is intended to be upgraded
        if not args.cardid:
            print_error("Enter a valid 'cardid' for the controller which needs to be upgraded")
        else:
            cardid = args.cardid

            # Get the list of all active controllers
            controller_upgrade_payload = None
            all_active_ctlr_list = get_all_controllers()

            for ctlr in all_active_ctlr_list:
                if cardid == int(ctlr['id']):
                    controller_upgrade_payload = ctlr
                    break

            if controller_upgrade_payload:
                ctlr_list = []
                ctlr_list.append(controller_upgrade_payload)

                controller_check(ctlr_list, True)

                # Upgrade the controllers
                print("\nTransfer the new SDA Image on Controller")
                controller_upgrade(ctlr_list)

                # Once transferred, the controller needs to be power cycled
                # This is done in two steps - power off, followed by power on

                print("\nController which underwent an SDA Image transfer is now being power cycled. Please wait...")
                power_off_controllers(ctlr_list)

                print("Controller has been powered off, will be powered back on in sometime. Please verify version once back online.")
                time.sleep(15)

                power_on_controllers(ctlr_list)
            else:
                print_error("Controller %s is not in an Active state, and cannot be upgraded" % str(cardid))

        print_error_report()

    elif args.mgmtupgrade:
        # Determine the mgmt which is intended to be upgraded
        if not args.cardid:
            print_error("Enter a valid 'cardid' for the management card which needs to be upgraded")
        else:
            cardid = args.cardid

            mgmt_card_id_list=[1,2]
            if cardid in mgmt_card_id_list:

                mgmt_check()

                # Determine whether we have a Standby card
                standby=get_passive_mgmt()

                standbycardnum=int(standby[4])

                if cardid == standbycardnum:
                    cmd="ping -c 1 {} > /dev/null".format(standby)
                    retcode, stdout = execute_cmd(cmd,standby)
                    if retcode!=0 :
                        str1= "There is no Standby Mgmt card  %s, nothing to upgrade."%(standby)
                        print_error(str1)
                    else:
                        transfer_sda_fw_image_on_mgmt(standby)
                        power_cycle_mgmt(standby)
                        print("Please verify the SDA component version once the standby mgmt card is back online.")
                else:
                    transfer_sda_fw_image_on_mgmt()
                    print("Please verify the SDA component version once the mgmt card is back online.")
                    power_cycle_mgmt()
            else:
                print_error("Invalid Management card number provided")

        print_error_report()

    elif args.chassisupgrade:
        print("All of the Active Controllers shall be upgraded, followed by Standby Mangement,")
        print("followed by Active Management, post which chassis shall be reloaded")

        # Check the SDA Size and Version on SDA Component on all Active Controllers
        ctlr_list = get_all_controllers()
        controller_check(ctlr_list, True)

        # Upgrade the controllers
        print("\nTransfer the new SDA Image on all Active Controllers")
        controller_upgrade(ctlr_list)

        # Now, check the SDA Size and Version of SDA Component on Mgmt cards
        mgmt_check()

        # Now, try and upgrade standby mgmt
        standby=get_passive_mgmt()
        cmd="ping -c 1 {} > /dev/null".format(standby)
        retcode, stdout = execute_cmd(cmd,standby)
        if retcode!=0 :
            str1= "There is no Standby Mgmt card  %s on this chassis."%(standby)
            print_error(str1)
        else:
            transfer_sda_fw_image_on_mgmt(standby)

        # Transfer the image on Active Mgmt as well
        transfer_sda_fw_image_on_mgmt()

        print("\nNew FW Image transferred successfully on all cards. The chassis shall now be reloaded")
        print("Please verify the version of all components once chassis is back online") 

        print_error_report()

        # Now that FW Images transferred with success, reload the chassis
        reload_chassis()


    elif args.h:
        help_summary()
    else:
        # No params provided - print the usage instructions
        help_summary()

if __name__ == "__main__":
    main()

