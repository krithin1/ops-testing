
lsblk | grep sda

mkdir -p /mnt

mount /dev/sda2 /mnt

sed -i 's/sda1/sda2/' /mnt/boot/grub/grub.cfg

grub-install --boot-directory=/mnt/boot /dev/sda

sync

