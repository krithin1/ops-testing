from checklist.WCserialformat import cmd, waca_serial_format
import sys
import os
import time

autil = "/usr/local/acadia/bin/atmelutil"


def vrev(card_v, slot, crd, crd_cmd, secure_shell=None):
    print("Version and Revision of %s in slot %d are : " % (crd, slot) + card_v[19:21])
    ans = str(input("Do you want to change it? (y/n)"))
    if ans == 'y' and secure_shell == 1:
        vrv = input("Enter Version and Revision: ")
        cmd("ssh mgmt2 ' " + autil + crd_cmd + card_v[0:19] + vrv + card_v[21:] + "'")
    elif ans == 'y':
        vrv = input("Enter Version and Revision: ")
        cmd(autil + crd_cmd + card_v[0:19] + vrv + card_v[21:])


while True:
    print("********Menu************")
    print("1.Sydney")
    print("2.Waca")
    print("3.Fabric")
    print("4.Exit")
    ch = str(input())
    os.system('clear')
    if ch == '4':
        sys.exit()
    elif ch == "1":
        # Sydney SY000187000054630223717OCT191JVD
        for i in (0, 1):
            ss = 0
            if i == 1:
                ss = 1
            sy = str(cmd(autil + " 1 misc %d " % i))
            vrev(sy, i, "Sydney", " 2 misc %d " % i, ss)

    elif ch == '2':
        # waca
        controller_count = int(input("Enter number of controllers"))
        for i in range(1, controller_count + 1):
            wc = str(waca_serial_format(str(cmd(autil + " 27 %d" % i + " | grep -i wc")))).split()
            vrev(wc[0], i, "Waca", " 28 %d WC " % i)
            vrev(wc[1], i, "Wankhede", " 28 %d WK " % i)
            wc = str(waca_serial_format(str(cmd(autil + " 27 %d" % (i + 10) + " | grep -i wc")))).split()
            vrev(wc[0], i + 10, "Waca", " 28 %d WC " % i)
            vrev(wc[1], i + 10, "Wankhede", " 28 %d WK " % i)
    elif ch == '3':
        # fab
        for i in (2,3):
            sy = str(cmd(autil + " 1 misc %d " % i))
            vrev(sy, i, "Newlands", " 2 misc %d " % i, )
    else:
        print("Error : Enter Valid input")
        time.sleep(2)
    os.system('clear')
