import sys
import subprocess


def cmd(c):
    try:
        q = subprocess.check_output(c, shell=True)
        r = q.decode("utf-8")
        return r
    except:
        return 0



f = open("test.txt", 'r')
f1 = f.read().splitlines()
for x in range(len(f1)):
	ns = cmd("nslookup %s | grep name | awk -F '\t' '{print $2}' | awk '{print $3}'" % f1[x])
	if ns:	
		print(f1[x] + " = "+ ns )
