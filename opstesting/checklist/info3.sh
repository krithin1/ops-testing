
echo "Mellanox:"
./do.sh $1 $2 "ethtool -i eth1 | grep -i firmware" | grep  PVL | cut -c 18-48
echo "Disk usage :"
./do.sh $1 $2 "df -h | grep sda1 " | grep '%' |cut -c 33-38
echo "SDA size :"
./do.sh $1 $2 "lsblk | grep -e 'sda.*disk'" | grep -v 'node\|mgmt' | awk '{print $4}' 
echo "BIOS version:"
./do.sh $1 $2 "dmidecode -s bios-version" | grep -v 'node\|mgmt'
echo "BIOS release date:"
./do.sh $1 $2 "dmidecode -s bios-release-date" | grep -v 'node\|mgmt'
echo "Memory info:"
./do.sh $1 $2 "cat /proc/meminfo | grep -i memtot " | grep Mem | cut -c 17-28
echo "CPU Speed :"
./do.sh $1 $2 "lscpu | grep 'CPU MHz' " | grep -v 'node\|mgmt' | awk '{print $3}'
