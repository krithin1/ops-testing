
echo powering off LEDs

/usr/local/acadia/bin/atmel_test "sled 0000 500 0 "

sleep 10s

echo powering on GREEN led and Blinking of Power button for 60s
/usr/local/acadia/bin/atmel_test "sled 5555 500 1 "
sleep 60s
echo powering on AMBER led and ID led  for 60s
/usr/local/acadia/bin/atmel_test "sled AAAA 500 0 "
sleep 60s
echo Rebooting atmel to restore led panel to normal state 
/usr/local/acadia/bin/atmel_test "reboot "
