


lsblk | grep sda
mkdir -p /mnt

mount /dev/sda1 /mnt

sed -i 's/sda2/sda1/' /mnt/boot/grub/grub.cfg

grub-install --boot-directory=/mnt/boot /dev/sda

sync

