
import argparse
import json
import os
import subprocess
import sys
import tarfile
import pvlclient
import logging
import requests
import datetime
import time
from datetime import datetime



time_zone = None
pvlLibHandle = None
EXT_PKGS='/data/external/ext_pkgs.json'
ERROR=[]

def print_error(msg):
    global ERROR
    ERROR.append(msg)
    #print("\033[31mERROR: %s \033[m"%msg)

def print_error_report():
    global ERROR
    if len(ERROR) > 0:
        print ("\033[31m\n***** ERROR REPORT ***** \033[m")
        for i in ERROR:
                print("\033[31mERROR: %s \033[m"%i)
    else:
        print ("\n***** No ERROR reported *****")
    

def setup_pvlclient(username, passord):
    global pvlLibHandle
    logger = logging.getLogger('test')
    hdlr = logging.FileHandler('/tmp/pvlclient.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    pvlLibHandle = pvlclient.New(u'2.0', u'127.0.0.1', 443, logger=logger, keep_alive=True)
    result = pvlLibHandle.auth.login(user=username, password=passord)
    #print (dir(result))
    

def execute_cmd(cmd , controller=None):

    stdout = None

    retcode = None
    try:
        def execute(cmd_str):
            #print ('Executing COMMAND %s'%cmd_str)
            result = subprocess.Popen(cmd_str ,shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            stdout = result.communicate()[0]
            retcode = result.returncode
            #if retcode != 0:
            #    print (stdout,retcode)
                #print (stdout)
                #print("\033[31mERROR: %s \033[m"%stdout)
            #    pass
            return retcode, (stdout.decode("utf-8")).strip().split('\n')

        if controller:
            cmd_str= 'ssh %s %s'%(controller,cmd)
            return execute(cmd_str)
        else:
            #print ("in elst")
            return execute(cmd)

    except  Exception as E:
        print (E)
        return retcode, stdout

def get_passive_mgmt():
    #cmd = '/usr/local/acadia/bin/atmelutil  4'
    #retcode, stdout = execute_cmd(cmd)
    #print (retcode, stdout)
    cmd = '/usr/local/acadia/bin/atmelutil  3'
    retcode, stdout = execute_cmd(cmd)
    #print (retcode, stdout)
    if retcode==0 and  'Slot:0' in stdout :
        return ('mgmt2')
    elif retcode==0 and  'Slot:1' in stdout :
        return ('mgmt1')
    else:
        return ('error')


def show_banner():
    result = pvlLibHandle.system.generic_api('GET', '/chassis/baseinfo')
    if result.error_code  != 0 :
        print_error("Failed to get Chassis base Info")
        assert(result.error_code  == 0)
    op_str = 'chassis_serial: %s \nchassis_name:   %s \ncurrent state:  %s\n'%(result.payload['chassis_serial'],
                                                  result.payload['chassis_name'], result.payload['chassis_displaystate'])
    print ("\nChassis summary")
    print (op_str)
    result = pvlLibHandle.system.generic_api('GET', '/chassis/upgrade-summary')
    
    #print (result.payload)
    if result.error_code  != 0 :
        print_error("Failed to get Upgrade summary")
        assert(result.error_code  == 0 )
    
    print ("Upgrade summary")
    lastup=result.payload[-1]
    op_str = 'From Version:   %s \nTo Version:     %s \nStatus:         %s\nTime Taken:     %s\n'%(
              lastup['from_version'], lastup['to_version'],lastup['status'],get_time_delta(lastup['startTime'],lastup['endTime']))
    
    print (op_str)
    

def get_time_delta(start,end):
    FMT = '%Y-%m-%d %H:%M:%S'
    start_time = (start.split('.')[0]).replace("==", " ")
    end_time = (end.split('.')[0]).replace("==", " ")
    return  datetime.strptime(end_time, FMT) - datetime.strptime(start_time, FMT)
    

def get_all_controllers(print_sum=True):
    result = pvlLibHandle.system.generic_api('GET', '/chassis/controllers?type=1')
    #print (result.payload)
    if result.error_code  != 0 :
        print_error("Failed to get controller Info")
        assert(result.error_code  == 0)
    #ctlr_data = json.loads(result.payload[0])
    ctlr_list=[]
    if print_sum:
        print ('Controller summary')
    for ctlr in result.payload:
        node= "Node%s"%ctlr['id']
        print ("Node%s    : %s"%(ctlr['id'],ctlr['displayState']))
        if ctlr['displayState'] == 'Active' :
            ctlr_list.append(ctlr)
        
    return ctlr_list 

def return_ext_data():
    pack_d =None
    with open(EXT_PKGS, 'r') as f:
        pack_d = json.load(f)
    return pack_d

def check_packages_on_mgmt(mgmt=None):
    pkgs = return_ext_data()
    
    if mgmt :
        print("\nRunning Package check on %s"%mgmt)
    else:
        print("\nRunning Package check on active mgmt")
        
    python_pkgs={}
    for i in pkgs['install']:
        if i['component'] == 'mgmt' and i['category'] == 'python':
            python_pkgs[i['pkg_name']]=i['version']

    cmd = 'pip  list'
    retcode, stdout = execute_cmd(cmd,mgmt)
    
    if retcode!=0 :
        str= "Failed to get Python packages info on %s"%mgmt if mgmt is not None else "Failed to get Python packages info"
        print_error(str)
        return
        #assert(retcode==0)
    not_found=[]
    for pkg in python_pkgs:   
        ret_pkg=python_pkgs.get(pkg,None)
        if not ret_pkg:
            not_found.append(pkg)

    if len(not_found) != 0:
        print_error('Below packages not found or version mismatch ')
        print (not_found)
    else:
        print("Package check done")
    if mgmt != None:
        print('checking time on passive management %s'%mgmt)
        check_time_on_controller(mgmt,'management')
        

def check_packages_on_controller(node):
    pkgs = return_ext_data()
    
    print("\n\nRunning Package check on controller %s"%node)
        
    #print (pkg_file)
    python_pkgs={}
    pman_pkgs={}
    for i in pkgs['install']:
        #print (i)
        if i['component'] == 'ctrl' and i['category'] == 'python':
            python_pkgs[i['pkg_name']]=i['version']
        elif i['component'] == 'ctrl' and i['category'] == 'pacman':
            pman_pkgs[i['pkg_name']]=i['version']

    # check for packman packages
    cmd = 'pacman -Q'
    retcode, stdout = execute_cmd(cmd,node)
    if retcode!=0 :
        str= "Failed to get packages list on controller %s"%node
        print_error(str)
        assert(retcode==0)

    #print (stdout
    for pkg_name in pman_pkgs.keys():
        #print (pkg_name,pman_pkgs[pkg_name])
        #if "%s %s"%(pkg_name,pman_pkgs[pkg_name]) not in stdout:
        #    print_error("package %s not found or version mismatch on controller %s"%(pkg_name,node))
        for pk in stdout:
            pk.startswith("%s %s"%(pkg_name,pman_pkgs[pkg_name]))
            break
        else:       
            print_error("package %s not found or version mismatch on controller %s"%(pkg_name,node))
        
    print("Package check done for controller %s"%node)


def check_process_on_controller(node):
    pkgs = return_ext_data()
            
    process_list=['/usr/bin/ntpd', '/usr/bin/rpcbind' ]
    # check for packman packages
    cmd = 'ps -aux'
    retcode, stdout = execute_cmd(cmd,node)
    #print(stdout)
    if retcode!=0 :
        str= "Failed to get packages list on controller %s"%node
        print_error(str)
        assert(retcode==0)

    for proc in process_list:
        for rproc in stdout:
            if proc in  rproc:
                break
        else:
            print_error("Process %s not running on controller %s"%(proc,node))

    cmd = 'systemctl status nfs-server.service'
    retcode, stdout = execute_cmd(cmd,node)
    #if retcode != 0 :
    #    print_error("Service nfs-server.service not found or disabled on controller %s"%(node))
    #else:
    for i in stdout :
        converted = ((i.encode('ascii', 'ignore').decode('ascii')).lstrip())
        #if ((i.encode('ascii', 'ignore').decode('ascii')).lstrip()).startswith('Active: active d'):
        if converted.startswith('Active: active'):
            #print ('FOUND')
            break
    else:
            print_error("Service nfs-server.service not found or disabled on controller %s"%(node))
            
    print('Process check done...')

def controller_check(ctlr_list):
    print("\n\n*****Starting postcheck on controllers*****")
    #print (ctlr_list)
    for ctlr in ctlr_list:
        node='node'+ctlr['id']
        check_packages_on_controller(node)
        check_process_on_controller(node)
        check_time_on_controller(node)
    print("\n\nDone with postcheck on controllers")

       
def check_time_on_controller(node, personality='controller'):
    cmd='date +%Y:%m:%d:%H:%M:%S_%Z'
    cmd= 'date  +"%Y-%m-%d==%H:%M:%S_%Z"'
    retcode, stdout = execute_cmd(cmd)
    if retcode!=0 :
        str1= "Failed to get date on active mgmt"
        print_error(str1)
        assert(retcode==0)

    mgdate, mgtzone = stdout[0].split('_')[0] ,stdout[0].split('_')[-1]
    #print (mgtzone,mgdate)
    # get controller date
    retcode, stdout = execute_cmd(cmd,node)
    if retcode!=0 :
        str1= "Failed to get date on %s %s"%(personality,node)
        print_error(str1)
        assert(retcode==0)

    cdate, ctzone = stdout[0].split('_')[0] ,stdout[0].split('_')[-1]
    #print (ctzone, cdate)
    if ctzone != mgtzone:
        print_error("Time zone on %s %s differ from mgmt"%(personality,node))
        return  
    tdiff = get_time_delta(mgdate, cdate)
    #print ((tdiff.total_seconds()))
    total_sec = tdiff.total_seconds()
    if total_sec > 300.0 or total_sec < -300.0:
        print_error("%s %s lags behing by %s seconds with management"%(personality,node,total_sec))
    print('Time & Time zone check done...')

    

def main():
    parser = argparse.ArgumentParser(description='verify packages.')
    parser.add_argument("--mgmt",action = 'store_true',help="verify presence of required packages on mgmt")
    parser.add_argument("--ctlr",action = 'store_true',help="verify presence of required packages on controller")
    parser.add_argument("--user",action = 'store_true', default='admin', help="chassis user name")
    parser.add_argument("--password",action = 'store_true', default='admin', help="chassis password")
    args = parser.parse_args()
    setup_pvlclient(args.user,args.password)
    show_banner()
    ctlr_list = get_all_controllers()
    #check_packages_on_controller('node4')
    #check_process_on_controller('node17')
    #print_error_report()
    #return 
    if args.ctlr:
        controller_check(ctlr_list)
    elif args.mgmt:
        check_packages_on_mgmt()
        check_packages_on_mgmt(get_passive_mgmt())
    else:
        check_packages_on_mgmt()
        check_packages_on_mgmt(get_passive_mgmt())
        controller_check(ctlr_list)

    print_error_report()
if __name__ == "__main__":
    main()

